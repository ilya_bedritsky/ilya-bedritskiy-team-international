﻿#region Copyright statement
// --------------------------------------------------------------
// Copyright (C) 1999-2016 Exclaimer Ltd. All Rights Reserved.
// No part of this source file may be copied and/or distributed 
// without the express permission of a director of Exclaimer Ltd
// ---------------------------------------------------------------
#endregion
using DeveloperTestInterfaces;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace DeveloperTest
{
    public sealed class DeveloperTestImplementationAsync : IDeveloperTestAsync
    {
        public async Task RunQuestionOne(ICharacterReader reader, IOutputResult output, CancellationToken cancellationToken)
        {
            var str = await ReadString(reader, cancellationToken);
            var parts = GetParts(str);
            WriteResults(parts, output);
        }

        private async Task<string> ReadString(ICharacterReader reader, CancellationToken cancellationToken)
        {
            var str = new StringBuilder();
            bool exists = true;

            using (reader)
            {
                while (exists)
                {
                    try
                    {
                        str.Append(await reader.GetNextCharAsync(cancellationToken));
                    }
                    catch (EndOfStreamException)
                    {
                        exists = false;
                    }
                }
            }

            return str.ToString();
        }

        private string[] GetParts(string str)
        {
            // delete all extra whitespaces
            var result = Regex.Replace(str.Trim(), @"\s+|--", " ");
            //remove all punctuation marks
            result = Regex.Replace(result, @"[^\P{P}-]+", "").ToLower();
            result = Regex.Replace(result, "`", "");

            return result.Split(' ');
        }

        private void WriteResults(string[] parts, IOutputResult output)
        {
            var groups = parts.GroupBy(s => s)
                .OrderByDescending(gr => gr.Count())
                .ThenBy(gr => gr.Key);

            foreach (var group in groups)
            {
                output.AddResult($"{group.Key} - {group.Count()}");
            }
        }

        public async Task RunQuestionTwo(ICharacterReader[] readers, IOutputResult output, CancellationToken cancellationToken)
        {
            var watch = new Stopwatch();
            watch.Start();

            var tasks = readers.AsParallel().WithDegreeOfParallelism(15)
                .Select(async reader => GetParts(await ReadString(reader, cancellationToken)));

            var result = (await Task.WhenAll(tasks)).SelectMany(parts => parts).ToArray();

            watch.Stop();
            Debug.WriteLine("Finished: {0}ms", watch.ElapsedMilliseconds);

            WriteResults(result.ToArray(), output);
        }
    }
}